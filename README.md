# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Avec maven : `mvn package`. 

Le jar généré est disponible dans /target. Un jar auto-exécutable avec les dépendances du projet est disponible (sous le nom `*-jar-with-dependencies.jar`).

## Exemple

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 

## Mise en place de docker

1) Crée l'image : docker image build -t simple-http-hello-world .
2) Crée le container et le mettre en route a partir de l'image : docker container run -p 8181:8181 --name simple-http-hello-world -d simple-http-hello-world
3) Tester l'accès au site avec une requête curl par exemple : curl -f --request GET --url 'http://127.0.0.1:8181'